#!/usr/bin/env groovy
// The above line is used to trigger correct syntax highlighting.

pipeline {
    // Lets Jenkins use Docker for us later.
    agent any    
        
    // If anything fails, the whole Pipeline stops.
    stages {
        stage('Build & Test') {   
            // Use golang.
            agent { docker { image 'golang' } }
            
            steps {                                           
                // Create our project directory.
                sh 'cd ${GOPATH}/src'
                sh 'mkdir -p ${GOPATH}/src/calc_app'
                
                // Copy all files in our Jenkins workspace to our project directory.                
                sh 'cp -r ${WORKSPACE}/* ${GOPATH}/src/calc_app'                        
            
        	//sh 'go get ./...'
        	sh 'go get -u github.com/go-sql-driver/mysql'
            
                // Build the app.
                sh 'go build'               
                
                // Remove cached test results.
                sh 'go clean -cache'
                
                // Run Unit Tests.
                sh 'go test ./... -v -short'    
            }            
        }
        
        stage('Docker') {         
            environment {
                // Extract the username and password of our credentials into "DOCKER_CREDENTIALS_USR" and "DOCKER_CREDENTIALS_PSW".
                // (NOTE 1: DOCKER_CREDENTIALS will be set to "your_username:your_password".)
                // The new variables will always be YOUR_VARIABLE_NAME + _USR and _PSW.
                // (NOTE 2: You can't print credentials in the pipeline for security reasons.)
                DOCKER_CREDENTIALS = credentials('98576b20-137c-4c61-80c5-61cea73d131c')
            }
        
            steps {                           
                // Use a scripted pipeline.
                script {
                    node {
                        def app
        
                        stage('Clone repository') {
                            checkout scm
                        }
        
                        stage('Build image') {    
                            // Expects that we have a file called "Dockerfile" in our project's root directory.
                            app = docker.build("${env.DOCKER_CREDENTIALS_USR}/calc-app-img")
                        }
                        
                        stage('Push image') {  
                            // Use the Credential ID of the Docker Hub Credentials we added to Jenkins.
                            docker.withRegistry('https://registry.hub.docker.com', '98576b20-137c-4c61-80c5-61cea73d131c') {                                
                                // Push image and tag it with our build number for versioning purposes.
                                app.push("${env.BUILD_NUMBER}")                      
                                
                                // Push the same image and tag it as the latest version (appears at the top of our version list).
                                app.push("latest")
                            }
                        }              
                    }                 
                }
            }
        }                  
    }        

    post {
        always {
            // Clean up our workspace.
            deleteDir()
        }
    }
}   

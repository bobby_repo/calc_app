package main

import (
    "net/http"
    "strconv"
    "io/ioutil"
    "net/url"
    "strings"
    "math"
    "encoding/json"
    "database/sql"
)

type Result struct {
    Operation string
    Result string
}

var dbHandle *sql.DB

func reportFunc (apiRes string, apiType string, dataString string, queryString string) (string) {
    var outStr string

    switch apiType {
    case "GET":
        myQuery, _ := url.ParseQuery(queryString)
        startDate := myQuery.Get("start_date")
        endDate := myQuery.Get("end_date")
        weekOfYear, _ := strconv.Atoi(myQuery.Get("week_of_year"))
        dateType := myQuery.Get("date_type")
        reportType := myQuery.Get("report_type")

        outStr = dbRead(dbHandle, startDate, endDate, weekOfYear, dateType, reportType)
    }

    return outStr

}

func roots(x float64, n float64) float64 {
    z := x / n
    for i := 0; i < 20; i++ {
        if math.Pow(z, n) == x {
            return z
        }
        z -= (math.Pow(z, n) - x) / (n * math.Pow(z, n-1))
    }
    return z
}

func calcFunc (apiRes string, apiType string, dataString string, queryString string) (string) {
    var outStr string
    var operation_result float64
    var Res Result

    switch apiType {
    case "GET":
        myQuery, _ := url.ParseQuery(queryString)
        var err error
        var operand1 float64
        var operand2 float64


        operand1, err = strconv.ParseFloat(myQuery.Get("operand1"), 64)
        operand2, err = strconv.ParseFloat(myQuery.Get("operand2"),  64)
        operation_type := myQuery.Get("operation_type")
        _ = err

        switch operation_type {
        case "add":
            Res.Operation = "addition(+)"
            operation_result = operand1 + operand2
        case "sub":
            Res.Operation = "subtraction(-)"
            operation_result = operand1 - operand2
        case "mul":
            Res.Operation = "multiplication(*)"
            operation_result = operand1 * operand2
        case "div":
            Res.Operation = "division(/)"
            operation_result = operand1 / operand2


        case "root":
            Res.Operation = "root of"
            operation_result = roots(operand1, operand2)
        case "exp":
            Res.Operation = "power of"
            operation_result = math.Pow(operand1, operand2)
        case "fraction":
            Res.Operation = "fraction"
            operation_result = operand1 / operand2
        }

        dbWrite (dbHandle, operation_type, operand1, operand2, "", operation_result)

    case "POST":

    case "PUT":

    case "PATCH":

    case "DELETE":

    }


    outStr = strconv.FormatFloat(operation_result, 'f', -1, 64)

    Res.Result = outStr

    jsonData, _ := json.Marshal(Res)

    jsonStr :=  string(jsonData)

    return jsonStr;
}

func  procCalc (w http.ResponseWriter, r *http.Request) {
    apiType := r.Method;
    apiResource := strings.ToLower(r.URL.Path);
    payloadData := ""
    queryString := ""
    var outPayloadJSON string

    //if apiType == http.MethodGet || apiType == http.MethodDelete  {
    myQuery, _ := url.ParseQuery(r.URL.RawQuery)
    for key, value := range myQuery {
        myQuery.Del(key)
        _ = value
        myQuery.Add(strings.ToLower(key), value[0])
    }
    newURLStr := myQuery.Encode()

    queryString = newURLStr //strings.ToLower(r.URL.RawQuery)

    //} else {
    body, err := ioutil.ReadAll(r.Body)

    payloadData = string(body)

    if err != nil {

    }
    //}



    switch apiResource {
    case "/test123":
        w.Write([]byte("My test"))
    case "/simple":
        outPayloadJSON = calcFunc(apiResource, apiType, payloadData, queryString)
    case "/complex":
        outPayloadJSON = calcFunc(apiResource, apiType, payloadData, queryString)
    }

    /*err := m.Unserialize([]byte(outPayload))
    _ = err
    response, _ := me.router(m)
    */
    //m.Serialize(1, "Get", "1.0", "")

    //w.Write([]byte(apiType + ": " + apiResource))
    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(outPayloadJSON))

    //io.WriteString(w, "test" + r.URL.String() );
}



func  procReports (w http.ResponseWriter, r *http.Request) {
    apiType := r.Method;
    apiResource := strings.ToLower(r.URL.Path);
    payloadData := ""
    queryString := ""
    var outPayloadJSON string

    //if apiType == http.MethodGet || apiType == http.MethodDelete  {
    myQuery, _ := url.ParseQuery(r.URL.RawQuery)
    for key, value := range myQuery {
        myQuery.Del(key)
        _ = value
        myQuery.Add(strings.ToLower(key), value[0])
    }
    newURLStr := myQuery.Encode()

    queryString = newURLStr //strings.ToLower(r.URL.RawQuery)

    //} else {
    body, err := ioutil.ReadAll(r.Body)

    payloadData = string(body)

    if err != nil {

    }
    //}



    switch apiResource {
    case "/reports":
        outPayloadJSON = reportFunc(apiResource, apiType, payloadData, queryString)
    }


    w.Header().Set("Content-Type", "application/json")
    w.Write([]byte(outPayloadJSON))

}

func  InitHTTPServer (tcpPort int) (int) {

    mux := http.NewServeMux()

    //mux.HandleFunc("/", myFunc)
    mux.HandleFunc("/simple", procCalc)
    mux.HandleFunc("/complex", procCalc)
    mux.HandleFunc("/reports", procReports)

    http.ListenAndServe(":"+strconv.Itoa(tcpPort), mux)

    return 0
}

func  StartHTTPServer (tcpPort int) {
    dbHandle = openDB("root:bobbyus77@tcp(127.0.0.1:3306)/calc")

    InitHTTPServer (tcpPort)

    //go InitHTTPServer (tcpPort)
}


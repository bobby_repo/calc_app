package main

import (
    "database/sql"
    "strconv"
    _ "github.com/go-sql-driver/mysql"
    "fmt"

    "encoding/json"
)



func openDB (dbURL string) (*sql.DB) {

    db, err := sql.Open("mysql", dbURL)
    if err != nil {
        panic(err.Error())
    }

    //defer db.Close()



    return db

}

func closeDB (dbHandle *sql.DB) {
    dbHandle.Close()
}

func dbWrite (dbHandle *sql.DB, operation string, op1 float64, op2 float64, user string, result float64) (bool) {
    insert, err := dbHandle.Query("INSERT INTO calc.entries (operation, op1, op2, user, result) VALUES ( '"+operation+"',"+
        strconv.FormatFloat(op1,'f', -1, 64)+","+
        strconv.FormatFloat(op2,'f', -1, 64)+",'',"+strconv.FormatFloat(result,'f', -1, 64)+" )")

    if err != nil {
        panic(err.Error())
        return false
    }

    defer insert.Close()

    return true

}

type reportResult struct {
    Columns []string
    Data []*dbList
}

type summaryResult struct {
    Columns []string
    Data []*dbSummary
}

type dbList struct {
    Operation string
    Op1 float32
    Op2 float32
    Result float32
    Op_datetime string
}

type dbSummary struct {
    Operation string
    Op_count int
}

//reportType = 0 - summary, 1 - full report
//dateType = 0 - day[startDate], 1 - week[weekOfYear], 2 - month [startDate], 3 - range [start/endDate]
func dbRead (dbHandle *sql.DB, startDate string, endDate string, weekOfYear int, dateRangeType string, reportType string) (string) {
    sqlQuery := ""
    var jsonStr []byte
    var summaryRes summaryResult
    var reportRes reportResult

    if reportType == "summary" {
        switch dateRangeType {
        case "day": sqlQuery = "select operation, count(id) from entries where date(op_datetime) = '"+startDate+"' group by operation"
        case "week": sqlQuery = "select operation, count(id) from entries where WEEK(op_datetime, 1) = " + strconv.Itoa(weekOfYear) + " group by operation"
        case "month": sqlQuery = "select operation, count(id) from entries where MONTH(op_datetime) = MONTH('"+startDate+"') group by operation"
        case "range": sqlQuery = "select operation, count(id) from entries where op_datetime between '"+startDate+"' and '"+endDate+" 23:59:59' group by operation"
        }
        //sqlQuery = "select operation, op1, op2, result, op_datetime from entries where op_datetime between '"+startDate+"' and '"+endDate+" 23:59:59'"
    } else if reportType == "report" {
        switch dateRangeType {
        case "day": sqlQuery = "select operation, op1, op2, result, op_datetime from entries where date(op_datetime) = '"+startDate+"'"
        case "week": sqlQuery = "select operation, op1, op2, result, op_datetime from entries where WEEK(op_datetime, 1) = " + strconv.Itoa(weekOfYear)
        case "month": sqlQuery = "select operation, op1, op2, result, op_datetime from entries where MONTH(op_datetime) = MONTH('"+startDate+"')"
        case "range": sqlQuery = "select operation, op1, op2, result, op_datetime from entries where op_datetime between '"+startDate+"' and '"+endDate+" 23:59:59'"
        }
        //sqlQuery = "select operation, count(id) from entries where op_datetime between '"+startDate+"' and '"+endDate+" 23:59:59' group by operation"
    }


    results, err := dbHandle.Query(sqlQuery)
    if err != nil {
        panic(err.Error()) // proper error handling instead of panic in your app
        return "error"
    }

    cols, err := results.Columns()
    sss, _:= json.Marshal(cols)
    ss := string(sss)
    _ = ss
    if err != nil {
        fmt.Println("Failed to get columns", err)
        return ""
    }
    _ = cols

    if reportType == "summary" {
        summaryRes.Columns = cols
        opEntries := make ([]*dbSummary, 0)

        for results.Next() {
            // for each row, scan the result into our tag composite object
            dbEntry := new (dbSummary)
            err = results.Scan(&dbEntry.Operation, &dbEntry.Op_count)
            opEntries = append(opEntries, dbEntry)
            if err != nil {
                panic(err.Error()) // proper error handling instead of panic in your app
            }
            // and then print out the tag's Name attribute
        }

        summaryRes.Data = opEntries

        jsonStr, _ = json.Marshal(reportRes)
    } else if reportType == "report" {
        j := 0

        //opDbEntries := make(opEntries, 1)
        reportRes.Columns = cols

        opEntries := make ([]*dbList, 0)

        for results.Next() {
            dbEntry := new (dbList)
            err = results.Scan(&dbEntry.Operation, &dbEntry.Op1, &dbEntry.Op2, &dbEntry.Result, &dbEntry.Op_datetime)
            opEntries = append(opEntries, dbEntry)
            //err = results.Scan(&databaseList)
            if err != nil {
                panic(err.Error()) // proper error handling instead of panic in your app
            }
            j++
            // and then print out the tag's Name attribute
        }

        reportRes.Data = opEntries

        jsonStr, _ = json.Marshal(reportRes)


    }


/*

    // Result is your slice string.
    rawResult := make([][]byte, len(cols))
    //result := make([][]string, 0, len(cols))
    result := make([][]string, 10)
    result1 := make([]string, len(cols))
    _ = result1
    //var resultArray []result

    dest := make([]interface{}, len(cols)) // A temporary interface{} slice
    for i, _ := range rawResult {
        dest[i] = &rawResult[i] // Put pointers to each string in the interface slice
    }

    j:=0
    for rows.Next() {
        err = rows.Scan(dest...)
        if err != nil {
            fmt.Println("Failed to scan row", err)
            return "error"
        }

        ///result = append(result, result1)
        //result = result[:j+1][:5]
        result[j] = make([]string, len(cols))
        for i, raw := range rawResult {

            if raw == nil {
                result[j][i] = "\\N"
            } else {
                result[j][i] = string(raw)
            }
        }
       //result = append(result, result1)
       //copy (result[j:j][2], result1)

        j++


        //fmt.Printf("%#v\n", result)
    }
    fmt.Println(json.Marshal(result))*/
/*
    if reportType == 0 {
        for results.Next() {
            var operation string
            var op_count int64
            // for each row, scan the result into our tag composite object
            err = results.Scan(&operation, &op_count)
            if err != nil {
                panic(err.Error()) // proper error handling instead of panic in your app
            }
            // and then print out the tag's Name attribute
        }
    } else {
        for results.Next() {
            var operation string
            var op1 float64
            var op2 float64
            var result float64
            var op_datetime string
            // for each row, scan the result into our tag composite object
            err = results.Scan(&operation, &op1, &op2, &result, &op_datetime)
            if err != nil {
                panic(err.Error()) // proper error handling instead of panic in your app
            }
            // and then print out the tag's Name attribute
        }

    }
*/

    /*colsStr, _ := json.Marshal(cols)
    reportRes.Columns = string(colsStr)
    reportRes.Data = string(jsonStr)

    jsonRep, _ := json.Marshal(reportRes)*/

    return string(jsonStr);//json.Marshal(result)

}
# Make a golang container using the "golang:1.10-alpine" docker image from the docker hub community (https://hub.docker.com/).
# We are using the official golang image.
# There are many tags to choose from. alpine is the simplest.
FROM golang:1.11.2-alpine3.8

RUN apk update && apk add git && go get github.com/go-sql-driver/mysql


# Expose our desired port.
EXPOSE 9999

# Create the proper directory.
RUN mkdir -p $GOPATH/src/calc_app

#RUN apk add git
# Copy app to the proper directory for building.
ADD . $GOPATH/src/calc_app

# Set the work directory.
WORKDIR $GOPATH/src/calc_app

# Run CMD commands.
RUN go get -d -v ./...
RUN go install -v ./...

# Provide defaults when running the container.
# These will be executed after the entrypoint.
# For example, if you ran docker run <image>,
# then the commands and parameters specified by CMD would be executed.
CMD ["calc_app"]

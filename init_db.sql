CREATE DATABASE `calc` /*!40100 DEFAULT CHARACTER SET latin1 */;

CREATE TABLE `entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operation` varchar(45) NOT NULL,
  `op_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `op1` double DEFAULT NULL,
  `op2` double DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `result` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `op_datetime` (`op_datetime`),
  KEY `op_index` (`operation`),
  KEY `op_datetime_operation` (`op_datetime`,`operation`),
  KEY `op_operation_datetime` (`operation`,`op_datetime`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

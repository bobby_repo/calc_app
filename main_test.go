package main

import (
	"testing"	
)

func TestTwoPlusTwo(t *testing.T) {
	result := 2 + 2
	expected := 4

	if expected != result {
		t.Error("expected:", expected, "got:", result)
	}
}
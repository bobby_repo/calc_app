package main

import (
	"fmt"


)

func main() {
	var (
		err error
	)
	_ = err

	fmt.Println("Listening on Port 9999...")

	StartHTTPServer (9999)


	// Run project with "go run main.go"
	// Access "localhost" in your browser.

	// Run tests with "go test ./... -v -short"
	// You can edit the tests to make them fail as well.
}
